#include <QCoreApplication>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QDebug>

void selectAllFromDepartement(QSqlDatabase db);
void selectWithInnerJoin(QSqlDatabase db);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QSqlDatabase db = QSqlDatabase::addDatabase(("QMYSQL"));
    db.setHostName("127.0.0.1");
    db.setPort(3306);
    db.setDatabaseName("psikotest");
    db.setUserName("psikotest");
    db.setPassword("psikotest");
    if(db.open()) {
        qDebug() << "Connected!";
    } else {
        qDebug() << "Failed to connect.";
        return 0;
    }

//    selectAllFromDepartement(db);
    selectWithInnerJoin(db);

    return a.exec();
}

void selectAllFromDepartement(QSqlDatabase db) {
    QString command = "SELECT name FROM department";
    QSqlQuery query(db);

    if(query.exec(command)) {
        while(query.next()) {
            QString name = query.value("name").toString();
            qDebug() << name;
        }
    }
}


void selectWithInnerJoin(QSqlDatabase db) {
    QString command = "SELECT my_user.username, department.name"
                      " AS deptname FROM "
                      " (SELECT * FROM user WHERE status = 0) AS my_user"
                      " INNER JOIN department ON department.id = my_user.departmentId";
    QSqlQuery query(db);
    if(query.exec(command)) {
        while(query.next()) {
            QString username = query.value("username").toString();
            QString department = query.value("deptname").toString();
            qDebug() << username << department;
        }
    }
}
