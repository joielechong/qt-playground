#include "mainwindow.h"
#include "ui_mainwindow.h"

#include<QDebug>
#include<QSqlQuery>
#include<QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    db = QSqlDatabase::addDatabase("QMYSQL");
    connectToDatabase(db);
}

MainWindow::~MainWindow()
{
    delete ui;
}

 void MainWindow::connectToDatabase(QSqlDatabase db) {
  db.setHostName("127.0.0.1");
  db.setPort(3306);
  db.setDatabaseName("psikotest");
  db.setUserName("psikotest");
  db.setPassword("psikotest");

  if(db.open()) {
      qDebug() << "Connected!";
  } else {
      qDebug() <<"Failed to connect.";
  }
}

void MainWindow::on_pushButton_clicked()
{
    QString username = ui->userNameEdit->text();
    QString password = ui->passwordEdit->text();
    qDebug() << username << password;

    if(isLoginOK(username, password)) {
        QMessageBox::information(this, "Login Success",
                                 "You have successfully logged in!");

        categoryWindow = new Category(db);
        categoryWindow->show();

    } else {
        QMessageBox::warning(this, "Login failed",
                             "Login failed. Please try again...");
    }
}

bool MainWindow::isLoginOK(QString username, QString password) {
    QString command = "SELECT * FROM user WHERE username = '" + username + "'"
            " AND password = '" + password + "'"
            " AND status = 0";

    QSqlQuery query(db);
    if(query.exec(command)) {
        if(query.size() > 0) {
            return true;
        }
    }
    return false;
}
