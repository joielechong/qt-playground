#include "category.h"
#include "ui_category.h"
#include<QtSql>
#include<QMessageBox>

Category::Category(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Category)
{
    ui->setupUi(this);

}

Category::Category(QSqlDatabase db) {
    ui->setupUi(this);
    this->db = db;

    model = new QSqlTableModel(ui->categoryTableView);
    model->setTable("category");

    if(!model->select()) {
        showError(model->lastError());
        return;
    }

    ui->categoryTableView->setModel(model);
    ui->categoryTableView->setColumnHidden(model->fieldIndex("id"), true);

}

Category::~Category()
{
    delete ui;
}

void Category::showError(const QSqlError &err) {
    QMessageBox::critical(this, "Unable to initialize database",
                          "Error initiliazing database: " + err.text());
}
