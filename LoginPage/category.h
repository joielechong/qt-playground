#ifndef CATEGORY_H
#define CATEGORY_H

#include <QWidget>
#include<QtSql/QSqlDatabase>
#include<QtSql/QSqlRelationalTableModel>

namespace Ui {
class Category;
}

class Category : public QWidget
{
    Q_OBJECT

public:
    explicit Category(QWidget *parent = 0);
    Category(QSqlDatabase db);
    ~Category();

private:
    Ui::Category *ui;
    QSqlDatabase db;

    QSqlTableModel *model;

    void showError(const QSqlError &err);
};

#endif // CATEGORY_H
